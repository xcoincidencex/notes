from random import randrange, shuffle

#implementation of quicksort algorythm
def quicksort(list, start, end):
  if start >= end:
    return
  
  pivot = randrange(start, end + 1)
  element = list[pivot]
  
  list[end], list[pivot] = list[pivot], list[end]
  
  less = start
  
  for i in range(start, end):
    if list[i] < pelement:
      list[i], list[less] = list[less], list[i]
      less += 1
  
  list[end], list[less] = list[less], list[end]
  
  quicksort(list, start, less - 1)
  quicksort(list, less + 1, end)

#implementation merge sort algorythm
def merge_sort(arr):
  if len(arr) <= 1:
    return arr
  mid_arr = len(arr) //2
  s = merge_sort(arr[0:mid_arr])
  e = merge_sort(arr[mid_arr:])
  return merge(s,e)

def merge(arr1,arr2):
  temp = []
  while (arr1 and arr2):
    if arr1[0] < arr2[0]:
      temp.append(arr1[0])
      arr1.pop(0)
    else:
      temp.append(arr2[0])
      arr2.pop(0)
  if arr1:
    temp += arr1
  if arr2:
    temp += arr2
  return temp


# test
list = [5,3,1,7,4,6,2,8]
shuffle(list)
print(merge_sort(list))
print("PRE SORT: ", list)
quicksort(list, 0, len(list) -1)
print("POST SORT: ", list)